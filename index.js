'use strict';

const required = val => {
    const isValid = !!val;
    const error = isValid ? null : 'This field is required';
    return {isValid, error};
};
const minLength = (val, min) => {
    const isValid = val.length >= min;
    const error = isValid ? null : `Length must be at least ${min}`;
    return {isValid, error};
};
const maxLength = (val, max) => {
    const isValid = val.length <= max;
    const error = isValid ? null : `Length must be no more than ${max}`;
    return {isValid, error};
};
const minValue = (val, min) => {
    const isValid = val >= min;
    const error = isValid ? null : `Value must be at least ${min}`;
    return {isValid, error};
};
const maxValue = (val, max) => {
    const isValid = val <= max;
    const error = isValid ? null : `Value must be no more than ${max}`;
    return {isValid, error};
};
const validator = {required, minLength, maxLength, minValue, maxValue};
const validate = (obj, cfg, isArray) => {
    if (cfg.type === 'object') {
        Object.keys(cfg['properties']).forEach(prop => validate(obj[prop], cfg['properties'][prop]));
    }
    else if (cfg.type === 'array') {
        const validity = [];
        if (cfg.rules) {
            Object.keys(cfg.rules).forEach(rule => validity.push(validator[rule](obj, cfg.rules[rule])));
        }
        cfg.status.isValid = validity.map(item => item.isValid).indexOf(false) < 0;
        cfg.status.error = validity.map(item => item.error).filter(item => item !== null);
        obj.forEach(item => validate(item, cfg.contentConfig, true));
    }
    else {
        const validity = [];
        if (cfg.rules) {
            Object.keys(cfg.rules).forEach(rule => validity.push(validator[rule](obj, cfg.rules[rule])));
        }
        if (!isArray) {
            cfg.status.isValid = validity.map(item => item.isValid).indexOf(false) < 0;
            cfg.status.error = validity.map(item => item.error).filter(item => item !== null);
        } else {
            cfg.status.isValid = Array.isArray(cfg.status.isValid) ? cfg.status.isValid : [];
            cfg.status.isValid.push(validity.map(item => item.isValid).indexOf(false) < 0);
            cfg.status.error = Array.isArray(cfg.status.error) ? cfg.status.error : [];
            cfg.status.error.push(validity.map(item => item.error).filter(item => item !== null));
        }
    }
};


const user = {
    firstName: 'Pesho',
    age: 7,
    pet: {
        name: 'Pachinkorukuru',
        age: 3
    },
    friends: [
        'Meche',
        'Zaio'
    ]
};

const cfg = {
    type: 'object',
    properties: {
        firstName: {
            type: 'string',
            rules: {
                minLength: 5,
                maxLength: 10
            },
            status: {
                isValid: false,
                error: null
            }
        },
        age: {
            type: 'number',
            rules: {
                minValue: 16
            },
            status: {
                isValid: false,
                error: null
            }
        },
        pet: {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    rules: {
                        minLength: 5,
                        maxLength: 10
                    },
                    status: {
                        isValid: false,
                        error: null
                    }
                },
                age: {
                    type: 'number',
                    rules: {
                        minValue: 2
                    },
                    status: {
                        isValid: false,
                        error: null
                    }
                }
            }
        },
        friends: {
            type: 'array',
            rules: {
                minLength: 3,
                maxLength: 10
            },
            status: {
                isValid: false,
                error: null
            },
            contentConfig: {
                type: 'string',
                rules: {
                    minLength: 5,
                    maxLength: 10
                },
                status: {
                    isValid: false,
                    error: null
                }

            }
        }
    }
};

validate(user, cfg);
console.log(cfg);

